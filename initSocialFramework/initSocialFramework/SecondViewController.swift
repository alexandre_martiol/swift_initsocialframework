//
//  SecondViewController.swift
//  initSocialFramework
//
//  Created by Alexandre Martinez Olmos on 18/3/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit
import Accounts
import Social

class SecondViewController: UIViewController {
    @IBOutlet weak var table: UITableView!
    var refreshControl:UIRefreshControl!
    var tweetsArray = NSArray()
    var imageDictionary:NSMutableDictionary!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "refreshTimeLine:", forControlEvents: UIControlEvents.ValueChanged)
        
        self.refreshTimeLine()
    }
    
    func refreshTimeLine(){
        if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)){
            var accountStore = ACAccountStore()
            var accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
            
            accountStore.requestAccessToAccountsWithType(accountType, options: nil, completion: {(granted,error) in
                if (granted == true) {
                    var arrayOfAccounts = accountStore.accountsWithAccountType(accountType)
                    var tempAccount = arrayOfAccounts.last as! ACAccount
                    var tweetURL = NSURL(string: "https://api.twitter.com/1.1/statuses/home_timeline.json")
                    var tweetRequest = SLRequest(forServiceType:SLServiceTypeTwitter, requestMethod: SLRequestMethod.GET, URL: tweetURL, parameters: nil)
                    tweetRequest.account = tempAccount
                    
                    tweetRequest.performRequestWithHandler({(responseData,urlResponse,error) in
                        if (error == nil) {
                            var jsonError:NSError?
                            var responseJSON = NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.AllowFragments, error: &jsonError) as! NSArray
                            
                            if (jsonError != nil) {
                                println("JSON ERROR")
                            }
                                
                            else {
                                self.tweetsArray = responseJSON
                                self.imageDictionary = NSMutableDictionary()
                                
                                dispatch_async(dispatch_get_main_queue(),{
                                    self.table.reloadData()})
                            }
                        }
                            
                        else {
                            //Error trying to access
                            println("No access granted")
                        }
                    })
                }
            })
        }
            
        else {
            //Show an alert view if there isn't a Twitter account
            let alertController = UIAlertController(title: "Error loading", message: "No Twitter account founded", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        refreshControl.endRefreshing()
    }
    
    // UITableViewDataSource methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweetsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("timelineCell") as! TimelineCell
        var currentTweet = self.tweetsArray.objectAtIndex(indexPath.row) as! NSDictionary
        var currentUser = currentTweet["user"] as! NSDictionary
        
        cell.usernameText.text = currentUser["name"] as? String
        cell.tweetText.text = currentTweet["text"] as! String
        var userName = cell.usernameText.text
        
        if ((self.imageDictionary[userName!]) != nil) {
            cell.tweetImage.image = (self.imageDictionary[userName!] as! UIImage)
        }
            
        else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                var imageURL = NSURL(string: currentUser.objectForKey("profile_image_url") as! String)
                var imageData = NSData(contentsOfURL: imageURL!)
                
                self.imageDictionary.setObject(UIImage(data:imageData!)!, forKey: userName!)
                
                (dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    cell.tweetImage.image = (self.imageDictionary[cell.usernameText.text!] as! UIImage)
                })
            })
        }
        
        return cell
    }
    
    // UITableViewDelegate methods
    /*func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

