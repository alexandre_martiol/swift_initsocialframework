//
//  TimelineCell.swift
//  initSocialFramework
//
//  Created by AlexM on 19/03/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class TimelineCell: UITableViewCell {
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var tweetImage: UIImageView!
    @IBOutlet weak var tweetText: UITextView!
}
