//
//  FirstViewController.swift
//  initSocialFramework
//
//  Created by Alexandre Martinez Olmos on 18/3/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit
import Accounts
import Social

class FirstViewController: UIViewController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var userDescription: UITextView!
    @IBOutlet weak var followersNumber: UILabel!
    @IBOutlet weak var followingNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func updateAction(sender: AnyObject) {
        if (usernameText.text != "") {
            checkUser()
        }
            
        else {
            //Show an alert view if the field is empty
            let alertController = UIAlertController(title: "Empty Username", message: "Write a username first", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func tweetAction(sender: AnyObject) {
        var twitterController = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
        
        twitterController.setInitialText("")
        
        var completionHandler:SLComposeViewControllerCompletionHandler = {(result) in
            twitterController.dismissViewControllerAnimated(true, completion: nil)
        
        switch(result) {
            case SLComposeViewControllerResult.Cancelled:
                println("The user has cancelled the tweet")
            case SLComposeViewControllerResult.Done:
                println("The user has tweeted")
            }
        }
        
        twitterController.completionHandler = completionHandler
        self.presentViewController(twitterController, animated: true, completion: nil)
    }
    
    func checkUser() {
        var accountStore = ACAccountStore()
        var accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
        
        accountStore.requestAccessToAccountsWithType(accountType, options: nil, completion: {(granted, error) in
            if (granted == true) {
                //Check if the device has linked a Twitter account
                var accounts = accountStore.accountsWithAccountType(accountType)
                
                if (accounts.count > 0){
                    var twitterAccount = accounts[0] as! ACAccount
                    var twitterInfoRequest = SLRequest(forServiceType: SLServiceTypeTwitter, requestMethod:SLRequestMethod.GET, URL: NSURL(string:"https://api.twitter.com/1.1/users/show.json"),parameters: NSDictionary(object:self.usernameText.text!, forKey: "screen_name") as! [NSObject : AnyObject])
                    
                    twitterInfoRequest.account = twitterAccount
                    twitterInfoRequest.performRequestWithHandler({(responseData,urlResponse,error) in
                        dispatch_async(dispatch_get_main_queue(), {() in
                        
                            //Check if the access has been rejected
                            if (urlResponse.statusCode == 429) {
                                println("Rate limit reached")
                            }
                                
                            //Check if there is an error
                            else if ((error) != nil) {
                                println("Error \(error.localizedDescription)")
                            }
                            
                            if (responseData != nil) {
                                var error: NSError?
                                var TWData = NSJSONSerialization.JSONObjectWithData(responseData, options:NSJSONReadingOptions.MutableLeaves, error: &error) as! NSDictionary
                                
                                println("TWDATA: \(TWData)")
                                
                                //Save the needed data
                                var followers = TWData.objectForKey("followers_count")!.integerValue!
                                var following = TWData.objectForKey("friends_count")!.integerValue!
                                var description = TWData.objectForKey("description") as! NSString
                                
                                self.followersNumber.text = "\(followers)"
                                self.followingNumber.text = "\(following)"
                                self.userDescription.text = description as! String
                                
                                //Download profile image
                                var profileImageStringURL = TWData.objectForKey("profile_image_url_https") as! NSString
                                profileImageStringURL = profileImageStringURL.stringByReplacingOccurrencesOfString("_normal", withString: "")
                                
                                var url = NSURL(string: profileImageStringURL as! String)
                                var data = NSData(contentsOfURL: url!)
                                self.userImage.image = UIImage(data: data!)
                            }
                        })
                    })
                }
            }
                
            else {
                println("No access granted")
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

